# Implement Jenkins using HELM

## Connect to the AKS 
~~~
az account set --subscription <subscription ID>
az aks get-credentials --resource-group <RESOURCE_GROUP_NAME> --name <AKS_CLUSTER_NAME>
~~~

## create namespace cicd
~~~
create namespace cicd
~~~

## add HELM repository named "Jenkins"
~~~
helm repo add jenkins https://charts.jenkins.io
~~~

## updates the local cache of available Helm charts from all added repositories
~~~
helm repo update
~~~

## upgrade a Helm release named "myjenkins"
~~~
helm upgrade --install myjenkins jenkins/jenkins --namespace cicd
~~~

## Check if it is ready:
~~~
kubectl get pods --namespace cicd
NAME          READY   STATUS    RESTARTS   AGE
myjenkins-0   2/2     Running   0          82s
~~~

## list all the secrets in the "cicd" namespace of the Kubernetes cluster
~~~
kubectl get secret --namespace cicd
~~~

## provide detailed information about the "myjenkins" secret in the "cicd" namespace
~~~
kubectl describe secret myjenkins --namespace cicd
~~~

## Retrieve the value of the "jenkins-admin-password" key from the "myjenkins" secret in the "cicd" namespace.
~~~
kubectl get secret myjenkins --namespace cicd -o jsonpath="{.data.jenkins-admin-password}" | base64 --decode
~~~

## Set up a port forwarding between your local machine's port 8080
~~~
kubectl --namespace cicd port-forward svc/myjenkins 8080:8080
~~~

## login with user and password:
~~~
user: admin
password- the output of the command:
kubectl get secret myjenkins --namespace cicd -o jsonpath="{.data.jenkins-admin-password}" | base64 --decode
~~~

## dowload plugins
## add values.yaml and apply it
~~~
jenkins:
  installPlugins:
    - kubernetes:1.30.0
    - git:4.7.0
    - workflow-aggregator:2.6


helm upgrade myjenkins jenkins/jenkins --namespace cicd -f values.yaml
~~~

## After install the plugins, do these commands:
~~~
kubectl get pods --namespace cicd

NAME          READY   STATUS    RESTARTS   AGE
myjenkins-0   2/2     Running   0          47m

kubectl delete pod myjenkins-0 --namespace cicd

pod "myjenkins-0" deleted
~~~

## Set up the port forwarding again and login with the same user and password
~~~
kubectl --namespace cicd port-forward svc/myjenkins 8080:8080
~~~

## add agent
~~~
pipeline {
    agent {
        kubernetes {
            inheritFrom 'maven'
        }
    }
    
    stages {
        stage('Build') {
            steps {
                sh 'echo "Building..."'
            }
        }
        stage('Test') {
            steps {
                sh 'echo "Testing..."'
            }
        }
        stage('Deploy') {
            steps {
                sh 'echo "Deploying..."'
            }
        }
    }
}
~~~


## check if the agent works
~~~
do this command in powershell:
kubectl get pods -n cicd --watch
~~~

## Add users
~~~
1. Log in to Jenkins using the existing administrator credentials.

2. Once you are logged in, click on "Manage Jenkins" in the Jenkins home page.

3. In the Manage Jenkins page, click on "Manage Users" from the available options.

4. Click on the "Create User" button to create a new user.

5. Fill in the user details for the first additional admin user:
~~~

## Clean UP:
~~~
kubectl delete pod myjenkins-0 --namespace cicd
helm uninstall myjenkins --namespace cicd
kubectl delete namespace cicd
helm repo remove jenkins
~~~
